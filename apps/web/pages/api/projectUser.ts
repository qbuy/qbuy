import prisma from "@/lib/prisma";
import { NextApiRequest, NextApiResponse } from "next";
export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse<any>,
) {
  const secretHeader = req.headers["X-Server-Secret"];

  if (secretHeader !== process.env.API_SERVER_SECRET_KEY) {
    return res.status(403).json({ message: "Доступ запрещен" });
  }
  if (req.method === "GET") {
    const { userId } = req.query;

    try {
      const response =
        await prisma.$queryRaw`SELECT projectId FROM ProjectUsers WHERE userId = ${userId}`;
      if (!response) {
        res.status(404).json([]);
      }
      res.status(200).json(response);
    } catch (error) {
      res
        .status(500)
        .json({ message: "Error fetching link data", error: error.message });
    }
  } else {
    res.setHeader("Allow", ["GET"]);
    res.status(405).end(`Method ${req.method} Not Allowed`);
  }
}
