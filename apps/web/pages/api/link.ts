import prisma from "@/lib/prisma";
import { NextApiRequest, NextApiResponse } from "next";
export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse<any>,
) {
  const secretHeader = req.headers["X-Server-Secret"];

  if (secretHeader !== process.env.API_SERVER_SECRET_KEY) {
    return res.status(403).json({ message: "Доступ запрещен" });
  }
  if (req.method === "GET") {
    const { domain, key } = req.query;

    try {
      const linkData = await prisma.link.findFirst({
        where: {
          domain: domain as string,
          key: decodeURIComponent(key as string),
        },
      });

      if (linkData) {
        res.status(200).json(linkData);
      } else {
        res.status(404).json([]);
      }
    } catch (error) {
      res
        .status(500)
        .json({ message: "Error fetching link data", error: error.message });
    }
  } else {
    res.setHeader("Allow", ["GET"]);
    res.status(405).end(`Method ${req.method} Not Allowed`);
  }
}
