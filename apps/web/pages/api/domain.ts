import prisma from "@/lib/prisma";
import { NextApiRequest, NextApiResponse } from "next";

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse<any>,
) {
  const secretHeader = req.headers["X-Server-Secret"];

  if (secretHeader !== process.env.API_SERVER_SECRET_KEY) {
    return res.status(403).json({ message: "Доступ запрещен" });
  }

  if (req.method === "GET") {
    const { slug } = req.query;

    try {
      const domainData = await prisma.domain.findUnique({
        where: {
          slug: slug as string,
        },
      });

      if (domainData) {
        res.status(200).json(domainData);
      } else {
        res.status(404).json([]);
      }
    } catch (error) {
      res
        .status(500)
        .json({ message: "Error fetching domain data", error: error.message });
    }
  } else {
    res.setHeader("Allow", ["GET"]);
    res.status(405).end(`Method ${req.method} Not Allowed`);
  }
}
