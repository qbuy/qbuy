import { Background } from "@dub/ui";
import { ReactNode } from "react";

export const runtime =
  process.env.NODE_ENV === "development" ? "nodejs" : "edge";

export default function AuthLayout({ children }: { children: ReactNode }) {
  return (
    <>
      <Background />
      <div className="relative z-10 flex h-screen w-screen justify-center">
        {children}
      </div>
    </>
  );
}
