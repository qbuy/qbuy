import { parse } from "@/lib/middleware/utils";
import { DUB_PROJECT_ID } from "@dub/utils";
import { getToken } from "next-auth/jwt";
import { NextRequest, NextResponse } from "next/server";
import { UserProps } from "../types";

export default async function AdminMiddleware(req: NextRequest) {
  const { path } = parse(req);
  let isAdmin = false;

  const session = (await getToken({
    req,
    secret: process.env.NEXTAUTH_SECRET,
  })) as {
    id?: string;
    email?: string;
    user?: UserProps;
  };

  const response = await fetch(
    `${process.env.API_BASE_URL}/api/projectUser?userId=${session?.user?.id}`,
    {
      method: "GET",
      headers: {
        "X-Server-Secret": process.env.API_SERVER_SECRET_KEY || "",
      },
    },
  );
  const rows = await response.json();
  if (rows?.[0]?.projectId === DUB_PROJECT_ID) {
    isAdmin = true;
  }

  if (path === "/login" && isAdmin) {
    return NextResponse.redirect(new URL("/", req.url));
  } else if (path !== "/login" && !isAdmin) {
    return NextResponse.redirect(new URL(`/login`, req.url));
  }

  return NextResponse.rewrite(
    new URL(`/admin.dub.co${path === "/" ? "" : path}`, req.url),
  );
}
