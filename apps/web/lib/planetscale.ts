import { DomainProps, ProjectProps } from "./types";
export const getProjectViaEdge = async (projectId: string) => {
  if (!process.env.API_BASE_URL) return null;

  const response = await fetch(
    `${process.env.API_BASE_URL}/api/project?projectId=${projectId}`,
    {
      method: "GET",
      headers: {
        "X-Server-Secret": process.env.API_SERVER_SECRET_KEY || "",
      },
    },
  );
  if (!response.ok) return null;
  const rows = await response.json();

  return rows && Array.isArray(rows) && rows.length > 0
    ? (rows[0] as ProjectProps)
    : null;
};

export const getDomainViaEdge = async (domain: string) => {
  if (!process.env.API_BASE_URL) return null;

  const response = await fetch(
    `${process.env.API_BASE_URL}/api/domain?slug=${domain}`,
    {
      method: "GET",
      headers: {
        "X-Server-Secret": process.env.API_SERVER_SECRET_KEY || "",
      },
    },
  );
  if (!response.ok) return null;
  const rows = await response.json();
  return rows && Array.isArray(rows) && rows.length > 0
    ? (rows[0] as DomainProps)
    : null;
};

export const getLinkViaEdge = async (domain: string, key: string) => {
  if (!process.env.API_BASE_URL) return null;

  const response = await fetch(
    `${process.env.API_BASE_URL}/api/link?domain=${domain}&key=${key}`,
    {
      method: "GET",
      headers: {
        "X-Server-Secret": process.env.API_SERVER_SECRET_KEY || "",
      },
    },
  );
  if (!response.ok) return null;
  const rows = await response.json();

  return rows && Array.isArray(rows) && rows.length > 0
    ? (rows[0] as {
        id: string;
        domain: string;
        key: string;
        url: string;
        proxy: number;
        title: string;
        description: string;
        image: string;
        rewrite: number;
        password: string | null;
        expiresAt: string | null;
        ios: string | null;
        android: string | null;
        geo: object | null;
        projectId: string;
        publicStats: number;
      })
    : null;
};

export async function getDomainOrLink({
  domain,
  key,
}: {
  domain: string;
  key?: string;
}) {
  if (!key || key === "_root") {
    const data = await getDomainViaEdge(domain);
    if (!data) return null;
    return {
      ...data,
      key: "_root",
      url: data?.target,
    };
  } else {
    return await getLinkViaEdge(domain, key);
  }
}
