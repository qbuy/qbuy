import { cn } from "@dub/utils";

export default function Logo({ className }: { className?: string }) {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      id="_\u0421\u043B\u043E\u0439_3"
      data-name="\u0421\u043B\u043E\u0439 3"
      viewBox="0 0 191 191"
      className={cn("h-10 w-10 text-black", className)}
    >
      <defs>
        <clipPath id="clippath">
          <path
            d="m188.51 103.28-85.22 85.23c-3.32 3.32-8.71 3.32-12.04 0l-50.96-50.95L0 97.26V0h97.27l91.24 91.24c3.32 3.32 3.32 8.71 0 12.04z"
            style={{
              fill: "none",
              strokeWidth: 0,
            }}
          />
        </clipPath>
        <clipPath id="clippath-1">
          <path
            d="M80.93 121.58 50.06 90.72l16.33-16.33 30.86 30.87 69.9-69.9 16.33 16.33-69.9 69.9c-9.02 9.02-23.65 9.02-32.67 0z"
            className="cls-2"
          />
        </clipPath>
        <style>{".cls-2{fill:#fff;stroke-width:0}"}</style>
      </defs>
      <path
        d="m188.51 103.28-85.22 85.23c-3.32 3.32-8.71 3.32-12.04 0l-50.96-50.95L0 97.26V0h97.27l91.24 91.24c3.32 3.32 3.32 8.71 0 12.04z"
        style={{
          strokeWidth: 0,
        }}
      />
      <g
        style={{
          clipPath: "url(#clippath)",
        }}
      >
        <path
          d="M80.93 121.58 50.06 90.72l16.33-16.33 30.86 30.87 69.9-69.9 16.33 16.33-69.9 69.9c-9.02 9.02-23.65 9.02-32.67 0z"
          style={{
            fill: "#fff",
          }}
        />
        <path
          d="m188.51 103.28-85.22 85.23c-3.32 3.32-8.71 3.32-12.04 0l-50.96-50.95L0 97.26V0h97.27l91.24 91.24c3.32 3.32 3.32 8.71 0 12.04z"
          className="cls-2"
          style={{
            clipPath: "url(#clippath-1)",
          }}
        />
      </g>
    </svg>
  );
}
